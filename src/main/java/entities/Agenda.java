/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Classe que implementa una agenda de noms amb diverses funcionalitats de tipus CRUD (Create, Read, Update, Delete)
 * @author manel
 */
public class Agenda {
    
    /* Els tipus de les col.leccions sempre han de ser classes (objectes),
    * mai poden ser dades primitives.*/
    private final ArrayList<String> llistaDeNoms; //ArrayList que conte objectes Strings

    //Constructor: crea una llista de noms buida
    public Agenda() {
        llistaDeNoms = new ArrayList(); //Instanciem l'objecte de tipus ArrayList
    }

    //Retorna el llistat de noms
    public ArrayList getLlistaDeNoms() {
        return llistaDeNoms;
    }

    //Retorna el total de noms del llistat
    public int getTotalElements() {
        //size() retorna el nombre d'elements del llistat
        return llistaDeNoms.size();
    }

    //Afegim un nom (element de l'ArrayList) al final de la llista ampliant la seva grandaria
    public void addNom(String pNom) {
        llistaDeNoms.add(pNom);
    }

    //Afegim un nom en una posició determinada de la llista. S'afegeix un nou element,
    //desplaçant els demés una posició a la dreta a partir de la posició passada com a paràmetre.
    public void addNomPosicio(int posicio, String pNom) {
        if (posicio >= 0 && posicio < this.getTotalElements()) {
            llistaDeNoms.add(posicio, pNom);
        } else {
            System.out.println("No existeix la posició introduida.");
        }
    }

    //Llegim un nom de la llista segons la seva posició
    public String getNom(int posicio) {
        if (posicio >= 0 && posicio < this.getTotalElements()) {
            //get(posicio) retorna l'element del llistat que ocupa la posició posicio
            return llistaDeNoms.get(posicio);
        } else {
            return "No existeix la posició solicitada";
        }
    }
    
    //Modifiquem un nom de la llista segons la seva posició
    public String setNom(int posicio, String nouNom) {
        if (posicio >= 0 && posicio < this.getTotalElements()) {
            //set(posicio, valor) modifica el contingut de la posició passada com a paràmetre.
            //amb el nou contingut pasat com a segon paràmetre
            return llistaDeNoms.set(posicio, nouNom);
        } else {
            return "No existeix la posició solicitada";
        }
    }

    //Elimina un nom del llistat (element de l'ArrayList) desplaçant els demés una 
    //posició a l'esquerra a partir de la posició passada com a paràmetre.
    public void removeNom(int posicio) { //Método
        if (posicio >= 0 && posicio < this.getTotalElements()) {
            //remove(posicio) l'element de la llista que ocupa la posició posicio
            llistaDeNoms.remove(posicio);
        }
    }

    //Mostra el contingut de la llista de noms
    public void imprimirNoms() {
        int comptador = 0; //Comptador de noms
        for (String element : llistaDeNoms) { //For extés
            comptador++;
            System.out.println("Element: " + element);
        }
        
        System.out.println("Total elements: " + comptador);
    }

    //Busquem en el llistat un nom en concret
    public void buscarNom(String pNom) {

        /* Un objecte iterator fa un recorregut sobre una copia de la col.lecció
         * a recorrer.
         * Iterator és una interficie genèrica.
         * Qualsevol col.lecció disposa del mètode iterator() què ens retorna un Iterator
         * de la col.lecció, en concret, ens retorna un objecte de tipus Iterator
         * amb una còpia de la col·lecció.
         * hasNext, retorna true si hi ha més elements en la col.lecció
         * next, retorna el següent objecte contingut en la col.lecció*/
        
        //Instanciem un objecte it de tipus iterator
        Iterator<String> it = llistaDeNoms.iterator();
        String elementActual;
        boolean isTrobat = false; //Semàfor per controlar si s'ha trobat el nom o no

        //Comprovem si l'objecte actual no és l'últim de la col.leció mitjançant el mètode hasNext.
        while (it.hasNext() && !isTrobat) {
            //referenciem al següent element de la llista mitjançant el mètode next()
            elementActual = it.next();
            System.out.println("L'element actual és: " + elementActual);
            
            // Les comparacions de tipus String poden ser exactes o no exactes
            // en aquest cas és no exacta
            if (elementActual.contains(pNom)) {
                System.out.println(pNom + " trobat !!");
                System.out.println("Elements no analitzats:");
                it.forEachRemaining(c-> System.out.println(c));
                isTrobat = true;
            }
        }

        if (isTrobat == (false)) { //Nom no trobat
            System.out.println(pNom + " no és de la col.lecció!!!");
        }
    }
    
}
