/*
 * Aquesta classe representa una curs d'alumnes identificat pel seu nom i la seva aula
 */

package entities;

/**
 * Classe que implementa un curs
 * @author fta & Manel
 */
public class Curs {
    private String nom;
    private String aula;
    
    public Curs(String pNom, String pAula){
        this.nom=pNom;
        this.aula=pAula;
    }
    
    public String getNom(){
        return this.nom;
    }
    
    public void setNom(String pNom){
        this.nom=pNom;
    }
    
    public String getAula(){
        return this.aula;
    }
    
    public void setAula(String pAula){
        this.aula = pAula;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Curs{");
        sb.append("nom=").append(nom);
        sb.append(", aula=").append(aula);
        sb.append('}');
        return sb.toString();
    }
    
    
    
}
