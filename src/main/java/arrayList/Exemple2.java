/*
 * Aquesta classe representa una llista de noms manejada amb la classe ArrayList.
 */
package arrayList;

/**
 *
 * @author fta
 * Darrera rev. 10/2020
 */
import entities.Agenda;
import java.util.Scanner;

// Classe que modela una llista de noms
public class Exemple2 {
    
    /***
     * Mètode principal
     * 
     * @param args 
     */
    public static void main(String[] args) {
        
        Scanner lector = new Scanner(System.in); //Lector teclat
        
        Agenda novaLlista = new Agenda(); //Creem llista noms

        //Iniciem lectura de dades i construcció del llistat
        System.out.println("Iniciem el programa");
        System.out.println("Introdueix nom 1:");
        String nom = lector.nextLine(); //Llegim nom 1
        novaLlista.addNom(nom); //Afegim nom 1 a llistat
        System.out.println("Introdueix nom 2:");
        nom = lector.nextLine(); //Llegim nom 2
        novaLlista.addNom(nom); //Afegim nom 2 a llistat
        System.out.println("Introdueix nom 3:");
        nom = lector.nextLine(); //Llegim nom 3
        novaLlista.addNom(nom); //Afegim nom 3 a llistat
        System.out.println("Gràcies.");

        System.out.println("La llista està formada per " + novaLlista.getTotalElements() + " elements");
        System.out.println("Element 1: " + novaLlista.getNom(0));
        System.out.println("Element 2: " + novaLlista.getNom(1));
        System.out.println("Element 3: " + novaLlista.getNom(2));

        System.out.println("Introdueix nom a afegir a posició 1:");
        nom = lector.nextLine(); //Llegim nom 4
        novaLlista.addNomPosicio(1, nom); //Afegim nom 4 a la posició 2
        System.out.println("Afegit la posició 1 (les col.leccions comencen a la posició zero)");

        //Mostrem el contingut de la llista amb un for normal
        System.out.println("La llista està formada per " + novaLlista.getTotalElements() + " elements");
        for (int i = 0; i < novaLlista.getTotalElements(); i++) {
            System.out.println("Posició " + i + ": " + novaLlista.getNom(i));
        }

        //Imprimi tots els noms per pantalla
        System.out.println("Mostrem tots els noms amb un for extes");
        novaLlista.imprimirNoms();
        
        //Eliminem el nom que ocupa la posició 2
        novaLlista.removeNom(2);
        //Imprimi tots els noms per pantalla
        System.out.println("Mostrem tots els noms, un cop eliminat el de posició 2");
        novaLlista.imprimirNoms();

        //Busquem nom
        System.out.println("Introdueix un nom per cercar en el llistat:");
        nom = lector.nextLine(); //Llegim nom a cercar
        novaLlista.buscarNom(nom);
    }
}
