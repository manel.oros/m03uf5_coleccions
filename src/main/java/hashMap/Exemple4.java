/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hashMap;

import entities.Agenda;
import entities.Curs;

/**
 *
 * @author manel
 */
public class Exemple4 {
    
     //Executem principal
    public static void main (String [ ] args) {
        //Definim llistatNomsAlumnat
        DadesCursos alumnatCopernic=new DadesCursos();
        
        //Definim cursos
        Curs smix1A=new Curs("SMIX-1A","1");
        Curs smix1B=new Curs("SMIX-1B","2");
        Curs smix2A=new Curs("SMIX-2A","3");
        
        //Definim Llistat d'alumnes
        Agenda alumnesSmix1A=new Agenda();
        Agenda alumnesSmix1B=new Agenda();
        Agenda alumnesSmix2A=new Agenda();
        
        //Afegim alumnes als llistats d'alumnes
        alumnesSmix1A.addNom("Pepito");
        alumnesSmix1A.addNom("Anna");
        alumnesSmix1A.addNom("Julia");
        alumnesSmix1B.addNom("Issac");
        alumnesSmix1B.addNom("Alberto");
        alumnesSmix2A.addNom("Pau");
        
        //Afegim els cursoso juntament amb el seus llistat d'alumnes al llistat de noms d'alumnes
        alumnatCopernic.putCurs(smix1A,alumnesSmix1A);
        alumnatCopernic.putCurs(smix1B,alumnesSmix1B);
        alumnatCopernic.putCurs(smix2A,alumnesSmix2A);
            
        //Imprimim alumnat
        alumnatCopernic.imprimirNoms();
        
        //Eliminem el grup de 1r de SMIX A
        alumnatCopernic.removeCurs(smix1A);
        
        //Tornem a imprimir l'alumnat
        alumnatCopernic.imprimirNoms();
    }
    
}
