/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hashMap;

/**
 * Exemple de gestió de dades mitjançant la classe DadesAlumnes i HashMap
 * 
 * @author manel
 */
public class Exemple3 {
    
   //Executem principal
    public static void main (String [ ] args) {
        
        DadesAlumnes smix=new DadesAlumnes(); //Nou conjunt d'alumnes
        
        //Inserim alumnes
        smix.putAlumne("00000000",18);
        smix.putAlumne("00000001",21);
        smix.putAlumne("00000002",32);
        smix.putAlumne("00000003",19);
        smix.putAlumne("00000004",18);
        smix.putAlumne("00000005",18);
        
        System.out.println("ALUMNES COPÈRNIC");
        
        System.out.println("El total d'alumnes de SMIX és: "+smix.totalAlumnes());
        
        //Imprimim les edats dels alumnes
        smix.imprimirEdats();
              
        //Eliminem l'alumne amb DNI 00000003
        smix.removeAlumne("00000003");
        
        System.out.println("Ara el total d'alumnes de SMIX és: "+smix.totalAlumnes());
        
        //Imprimim les edats dels alumnes
        smix.imprimirEdats();
        
        //Afegim un nou alumne que ja existeix.
        smix.putAlumne("00000005",5);
        
        //Afegim un nou alumne que no existeix.
        smix.putAlumne("00000007",99);
        
        //Modifiquem l'edat de l'alumne 000000001
        smix.setEdat("00000001",44);
        
        System.out.println("Ara el total d'alumnes de SMIX és: "+smix.totalAlumnes());
        
        //Imprimim les edats dels alumnes
        smix.imprimirEdats();
    }  
    
}
