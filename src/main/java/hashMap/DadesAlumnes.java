 /*
 * Classe que defineix un cunjunt d'alumnes. Per cada alumne codi (clau) i edat (valor)
 */
package hashMap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Classe que mitjançant un MashMap emmagatzema un DNI (clau) i una edat (valor)
 * @author fta & Manel
 */
public class DadesAlumnes {
    
    /***
     * Hash que emmagatzema el DNI d'un alumne i la seva edat
     */
    HashMap<String,Integer> hashMapAlumnes;
    
    //Constructor per crear un conjunt d'alumnes buit
    public DadesAlumnes(){
        hashMapAlumnes=new HashMap<>( );
    }
    
    //Total d'alumnes
    public int totalAlumnes() {
        //size() retorna el nombre d'elements del HashMap
        return hashMapAlumnes.size(); 
    }
    
    //Inserim un alumne en el conjunt d'alumnes
    public void putAlumne(String dni,Integer edat){
        if(!hashMapAlumnes.containsKey(dni)){ //Si no existeix la clau...
            //put(clau,valor) afegeix un nou element al HashMap amb clau=dni perquè no existeix
            hashMapAlumnes.put(dni,edat);
        }else{
            System.out.println("El dni introduit ja existeix.");
        }
    }
       
    //Llegim l'edat d'un alumne determinat
    //Integer per poder retornar null, ja què una dada primitiva no pot ser null, però un objecte si.
    public Integer getEdat(String dni) { 
        if (hashMapAlumnes.containsKey(dni)) {
            //get(clau) retorna el valor associat a la clau
            return hashMapAlumnes.get(dni); 
        }else{
            return null;
        }
    }
    
    //Modifiquem l'edat d'un alumne determinat per l'edat passada com a paràmetre
    //Integer per poder retornar null, ja què una dada primitiva no pot ser null, però un objecte si.
    public void setEdat(String dni, int novaEdat) { 
        if (hashMapAlumnes.containsKey(dni)) { //Si existeix l'edat...
            //put(clau,valor) modifica el valor associat a la clau perquè existeix
            hashMapAlumnes.put(dni,novaEdat); 
        }else{
            System.out.println("L'alumne no existeix!!!");
        }
    }
    
    //Elimina un alumne del conjut d'alumnes
    public void removeAlumne(String dni) {
        if (hashMapAlumnes.containsKey(dni)) {
            //remove(posicio) l'element de la llista que ocupa la posició posicio
            hashMapAlumnes.remove(dni); 
        }
    }
    
    //Imprimim les edats dels alumnes
    public void imprimirEdats(){
        //KeySet() retorna un conjunt amb les claus del HashMap passant a ser una 
        //col.lecció ietrable        
        Set dnis = hashMapAlumnes.keySet(); //Conjunt dels dnis dels alumnes
        //Creem un iterador per recorre els alumnes
        Iterator<String> dni = dnis.iterator();
       
        while(dni.hasNext()){ //Mentres el dni no sigui l'últim...
            String alumneActual=dni.next();
            System.out.println("L'edat de l'alumne amb DNI "+alumneActual+ " és de "+getEdat(alumneActual)); 
        }        
        
    }
}
