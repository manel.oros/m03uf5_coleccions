/*
 * Aquesta classe representa una col.lecció de noms d'alumnes d'un curs determinat,
 * guardats en un HashMap.
 */

package hashMap;

/**
 * Classe que emmagatzema cursos amb els seus alumnes en un HashMap
 * @author fta
 */
import entities.Curs;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import entities.Agenda;


public class DadesCursos {
    
    /***
     * HashMap on el curs és la clau i l'agenda d'alumnes és el valor
     */
    HashMap<Curs,Agenda> noms;
    
    //Constructor per crear un directori d'alumnes buit
    public DadesCursos(){
        noms=new HashMap<>( );
    }
    
    //Total de cursoso guardats en el directori
    public int getTotalCursos() {
        //size() retorna el nombre d'elements del HashMap
        return noms.size(); 
    }
    
    //Insertem un curs amb el nom dels seus alumnes en el directori
    public void putCurs(Curs curs,Agenda alumnes){
        if(!noms.containsKey(curs)){ //Si existeix la clau
            noms.put(curs,alumnes);
        }else{
            System.out.println("El curs introduit ja existeix.");
        }
    }
    
    //Llegim els noms d'un curs determinat
    public Agenda getNoms(Curs curs) {
        if (noms.containsKey(curs)) {
            //get(clau) retorna el valor associat a la clau
            return noms.get(curs); 
        }else{
            return null;
        }
    }
    
    //Elimina un curs del directori
    public void removeCurs(Curs curs) { //Método
        if (noms.containsKey(curs)) {
            //remove(posicio) l'element de la llista que ocupa la posició posicio
            noms.remove(curs); 
        }
    }
    
    //Imprimim els noms dels cursoso
    public void imprimirNoms(){
        //KeySet() retorna un conjunt amb les claus del HashMap passant a ser una 
        //col.lecció ietrable        
        Set cursos = noms.keySet(); //Conjunt dels cursos del directori
        //Creem un iterador per recorre els cursoso
        Iterator<Curs> curs = cursos.iterator();
        
        System.out.println("ALUMNES COPÈRNIC");
        while(curs.hasNext()){
            Curs cursActual=curs.next();
            System.out.println(cursActual + ": "); 
            getNoms(cursActual).imprimirNoms();
        }        
        
    }
}
